﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesModels
{
    public enum Freq { day = 0, month = 1, quarter = 2, year = 3, constant = 4, undetermined = 10 }

    public enum Transf { sum = 0, average = 1, max = 2, min = 3, first = 4, last = 5,  undetermined = 10 }
}
