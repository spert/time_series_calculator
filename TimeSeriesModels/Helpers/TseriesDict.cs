﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeSeriesModels
{
    public class TseriesDict
    {
        readonly int _id;
        readonly int _firstPeriod;
        readonly int _lastPeriod;
        readonly Freq _freq;
        readonly Dictionary<DateTime, double> _obs;
        readonly bool _isConstant;        

        public TseriesDict(double constant)
        {
            _isConstant = true;
            _obs = new Dictionary<DateTime, double>();
            _obs.Add(DateTime.MinValue, constant);

        }

        public TseriesDict(int id, Freq freq, Dictionary<DateTime, double> obs)
        {
            _id = id;
            _freq = freq;
            _obs = obs;
            _isConstant = false;
        }

        public Dictionary<DateTime, double> Obs { get { return _obs; } }        
        public int Id { get { return _id; } }
        public Freq Freq { get { return _freq; } }
        public int FirstPeriod { get { return _obs.Keys.First().DateTimeToInt(_freq); } } 
        public int LastPeriod { get { return _obs.Keys.Last().DateTimeToInt(_freq); } }
        public DateTime FirstPeriodDate { get { return _obs.Keys.First(); } } 
        public DateTime LastPeriodDate { get { return _obs.Keys.Last(); } } 
        public override string ToString()
        {
            string s = " ------------------- " + Environment.NewLine;
            s = s + "Dict Series    : " + Environment.NewLine;
            s = s + "Frequency      : " + _freq.ToString() + Environment.NewLine;
            s = s + "FirstPeriod    : " + _firstPeriod.ToString() + Environment.NewLine;
            s = s + "FirstPeriodDate: " + FirstPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "LastPeriodDate : " + LastPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "# Obervations  : " + _obs.Count.ToString() + Environment.NewLine;


            for (int i = 0; i < _obs.Count; i++)
            {
                if (i < 3 && _obs.Count >= 7)
                {
                    s = s + (i).ToString() + " : " + _obs.ElementAt(i).Key.ToString("dd.MM.yyyy") + " : " + _obs.ElementAt(i).Value.ToString() + Environment.NewLine;
                }
                if (i == 3 && _obs.Count >= 7)
                {
                    s = s + "..." + Environment.NewLine;
                }
                if (i > _obs.Count - 4 && _obs.Count >= 7)
                {
                    s = s + (i).ToString() + " : " + _obs.ElementAt(i).Key.ToString("dd.MM.yyyy") + " : " + _obs.ElementAt(i).Value.ToString() + Environment.NewLine;
                }
                if (_obs.Count < 7)
                {
                    s = s + i.ToString() + " : " + _obs.ElementAt(i).Key.ToString("dd.MM.yyyy") + " : " + _obs.ElementAt(i).Value + Environment.NewLine;
                }
            }

            return s;
        }

    }
}
