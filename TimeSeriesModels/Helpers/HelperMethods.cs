﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TimeSeriesModels
{

    public static class HelperMethods
    {
        public static TseriesDict GenerateDictSeries(int id, DateTime firstPeriod, DateTime lastPeriod, Freq Frequency)
        {
            Dictionary<DateTime, double> dict = new Dictionary<DateTime, double>();
            int start = 0;

            Random r = new Random();

            if (Frequency == Freq.day)
            {
                start = (firstPeriod - new DateTime(1970, 1, 1)).Days + 1;
                int daysToEnd = (lastPeriod - firstPeriod).Days;

                for (int i = 0; i <= daysToEnd; i++)
                {
                    if (firstPeriod.AddDays(i).DayOfWeek == DayOfWeek.Saturday || firstPeriod.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                        dict.Add(firstPeriod.AddDays(i), double.NaN);
                    else
                        dict.Add(firstPeriod.AddDays(i), Math.Round((r.NextDouble() * 100000)) / 100);
                }
            }

            if (Frequency == Freq.month)
            {
                start = firstPeriod.MonthlyDifference(new DateTime(1970, 1, 1)) + 1;
                int monthsToEnd = lastPeriod.MonthlyDifference(firstPeriod);

                for (int i = 0; i <= monthsToEnd; i++)
                {
                    dict.Add(firstPeriod.AddMonths(i).GetLastDayOfMonth(0), (r.NextDouble() * 100000) / 100);
                }
            }

            if (Frequency == Freq.quarter)
            {
                start = firstPeriod.QuarterlyDifference(new DateTime(1970, 1, 1)) + 1;
                int quarters = lastPeriod.QuarterlyDifference(firstPeriod);

                for (int i = 0; i <= quarters; i++)
                {
                    dict.Add(firstPeriod.AddMonths(i * 3).GetLastDayOfQuarter(0), (r.NextDouble() * 100000) / 100);
                }
            }

            if (Frequency == Freq.year)
            {
                start = firstPeriod.YearlyDifference(new DateTime(1970, 1, 1)) + 1;
                var years = lastPeriod.YearlyDifference(firstPeriod);

                for (int i = 0; i <= years; i++)
                {
                    dict.Add(firstPeriod.AddMonths(i * 12).GetLastDayOfYear(0), (r.NextDouble() * 100000) / 100);
                }
            }

            return new TseriesDict(id, Frequency, dict);
        }

        public static void ExportSeriesToCsvTable(List<TseriesDict> dailyDictSeries, string filePath)
        {
            Dictionary<DateTime, string> result = new Dictionary<DateTime, string>();

            var dates = dailyDictSeries.SelectMany(x => x.Obs.Keys).Distinct().OrderBy(k => k).ToList();

            foreach (var d in dates)
                result.Add(d, "");

            foreach (var d in dates)
            {
                for (int i = 0; i < dailyDictSeries.Count; i++)
                {
                    if (dailyDictSeries[i].Obs.ContainsKey(d))
                        result[d] = result[d] + ";" + dailyDictSeries[i].Obs[d].ToString();
                    else
                        result[d] = result[d] + ";NaN";
                }
            }

            string header = "dates";
            for (int i = 0; i < dailyDictSeries.Count; i++)
            {
                header = header + ";s" + i.ToString();
            }

            String csv = String.Join(Environment.NewLine, result.Select(d => d.Key.ToShortDateString() + d.Value));
            System.IO.File.WriteAllText(filePath, string.Concat(header, Environment.NewLine, csv));

        }

        public static Tseries ConvertDictSeriesToObjectSeries(TseriesDict t)
        {
            Tseries ts = new Tseries(t.Id, t.FirstPeriod, t.Freq, (new List<double>(t.Obs.Values)).ToArray());
            return ts;
        }

        public static TseriesDict ConvertObSeriesToDictSeries(Tseries t)
        {
            Dictionary<DateTime, double> dict = new Dictionary<DateTime, double>();

            for (int i = 0; i < t.Obs.Length; i++)
            {
                dict.Add((t.FirstPeriod + i).IntToDateTime(t.Freq), t.Obs[i]);
            }

            return new TseriesDict(t.Id, t.Freq, dict);
        }


        public static bool TimeSeriesAreEqual(Tseries t1, Tseries t2)
        {

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("First period is difefrent: " + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.Obs.Length != t2.Obs.Length)
                throw new Exception("Number of observations is diffrent: " + t1.Obs.GetUpperBound(0) + " / " + t2.Obs.GetUpperBound(0));

            for (int i = 0; i < t1.Obs.Length; i++)
            {
                if (Math.Round(t1.Obs[i], 4) != Math.Round(t2.Obs[i], 4))
                    throw new Exception("Observation number " + i + "  is diffrent");
            }

            if (t1.Freq != t2.Freq)
                throw new Exception("Frequency is diffrent" + t1.Freq + " / " + t2.Freq);

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("Firstperiod is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.LastPeriod != t2.LastPeriod)
                throw new Exception("Lastperiod is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.FirstPeriodDate != t2.FirstPeriodDate)
                throw new Exception("FirstPeriodDate is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.LastPeriodDate != t2.LastPeriodDate)
                throw new Exception("LastPeriodDate is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            return true;

        }

        public static bool TimeSeriesAreEqual(TseriesDict t1, Tseries t2)
        {
            double[] dict = (new List<double>(t1.Obs.Values)).ToArray();

            if (dict.Length != t2.Obs.Length)
                throw new Exception("Number of observations is diffrent: " + dict.GetUpperBound(0) + " / " + t2.Obs.GetUpperBound(0));

            for (int i = 0; i < dict.Length; i++)
            {
                if (!double.IsNaN(dict[i]) && !double.IsNaN(t2.Obs[i]))
                {
                    if (Math.Round(dict[i], 4) != Math.Round(t2.Obs[i], 4))
                        throw new Exception("Observation number " + i + "  is diffrent");
                }
            }

            if (t1.Freq != t2.Freq)
                throw new Exception("Frequency is diffrent" + t1.Freq + " / " + t2.Freq);

            if (t1.FirstPeriod != t2.FirstPeriod)
                throw new Exception("Firstperiod is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.LastPeriod != t2.LastPeriod)
                throw new Exception("Lastperiod is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.FirstPeriodDate != t2.FirstPeriodDate)
                throw new Exception("FirstPeriodDate is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            if (t1.LastPeriodDate != t2.LastPeriodDate)
                throw new Exception("LastPeriodDate is diffrent" + t1.FirstPeriod + " / " + t2.FirstPeriod);

            return true;

        }

        public static bool TimeSeriesAreEqual(Tseries t2, TseriesDict t1)
        {
            return TimeSeriesAreEqual(t1, t2);
        }


    }
}


