﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using TimeSeriesModels;

namespace TimeSeriesModels
{
    public static class TSeriesExtensions
    {
        public static DateTime GetLastDayOfMonth(this DateTime dateTime, int months)
        {
            return ((new DateTime(dateTime.Year, dateTime.Month, 1)).AddMonths(1 + months).AddDays(-1));
        }

        public static DateTime GetFirstDayOfMonth(this DateTime dateTime, int months)
        {
            return (new DateTime(dateTime.Year, dateTime.Month, 1)).AddMonths(months);
        }

        public static DateTime GetLastDayOfQuarter(this DateTime dateTime, int quarters)
        {
            DateTime _dateTime = dateTime.GetLastDayOfMonth(3 * quarters);
            int m = 0;

            if (_dateTime.Month >= 1 && _dateTime.Month <= 3)
                m = 3;
            else if (_dateTime.Month >= 4 && _dateTime.Month <= 6)
                m = 6;
            else if (_dateTime.Month >= 7 && _dateTime.Month <= 9)
                m = 9;
            else if (_dateTime.Month >= 10 && _dateTime.Month <= 12)
                m = 12;

            return new DateTime(_dateTime.Year, m, DateTime.DaysInMonth(_dateTime.Year, m));
        }

        public static DateTime GetFirstDayOfQuarter(this DateTime dateTime, int quarters)
        {
            DateTime _dateTime = dateTime.GetFirstDayOfMonth(3 * quarters);
            int m = 0;

            if (_dateTime.Month >= 1 && _dateTime.Month <= 3)
                m = 1;
            else if (_dateTime.Month >= 4 && _dateTime.Month <= 6)
                m = 4;
            else if (_dateTime.Month >= 7 && _dateTime.Month <= 9)
                m = 7;
            else if (_dateTime.Month >= 10 && _dateTime.Month <= 12)
                m = 10;

            return new DateTime(_dateTime.Year, m, 1);
        }




        public static DateTime GetLastDayOfYear(this DateTime dateTime, int years)
        {
            return new DateTime(dateTime.Year + years, 12, 31);
        }

        public static DateTime GetFirstDayOfYear(this DateTime dateTime, int years)
        {
            return new DateTime(dateTime.Year + years, 1, 1);
        }

        public static int DailyDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue - rValue).Days;
        }

        public static int DailyDifferenceFull(this DateTime lValue, DateTime rValue)
        {
            return DailyDifference(lValue, rValue);
        }

        public static int MonthlyDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }

        public static Tuple<DateTime, DateTime, int, int> MonthlyDifferenceFull(this DateTime end, DateTime beg)
        {
            DateTime _beg = beg;
            DateTime _end = end;

            if (!beg.IsFullPeriodDown(Freq.month))
                _beg = beg.GetLastDayOfMonth(1);

            if (!end.IsFullPeriodUp(Freq.month))
                _end = end.GetLastDayOfMonth(-1);

            return new Tuple<DateTime, DateTime, int, int>(_beg, _end, _beg.DateTimeToInt(Freq.month), _end.DateTimeToInt(Freq.month));
        }

        public static int QuarterlyDifference(this DateTime lValue, DateTime rValue)
        {
            return lValue.GetLastDayOfQuarter(0).MonthlyDifference(rValue.GetLastDayOfQuarter(0)) / 3;
        }

        public static Tuple<DateTime, DateTime, int, int> QuarterlyDifferenceFull(this DateTime higherPeriod, DateTime lowerPeriod)
        {
            DateTime _hp = higherPeriod;
            DateTime _lp = lowerPeriod;

            if (!lowerPeriod.IsFullPeriodDown(Freq.quarter))
                _lp = lowerPeriod.GetLastDayOfQuarter(1);

            if (!higherPeriod.IsFullPeriodUp(Freq.quarter))
                _hp = higherPeriod.GetLastDayOfQuarter(-1);

            return new Tuple<DateTime, DateTime, int, int>(_lp,_hp, _lp.DateTimeToInt(Freq.quarter), _hp.DateTimeToInt(Freq.quarter));
        }

        public static int YearlyDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Year - rValue.Year);
        }

        public static Tuple<DateTime, DateTime, int, int> YearlyDifferenceFull(this DateTime higherPeriod, DateTime lowerPeriod)
        {
            DateTime _hp = higherPeriod;
            DateTime _lp = lowerPeriod;

            if (!lowerPeriod.IsFullPeriodDown(Freq.year))
                _lp = lowerPeriod.GetLastDayOfYear(1);

            if (!higherPeriod.IsFullPeriodUp(Freq.year))
                _hp = higherPeriod.GetLastDayOfYear(-1);

            return new Tuple<DateTime, DateTime, int, int>(_lp, _hp, _lp.DateTimeToInt(Freq.year), _hp.DateTimeToInt(Freq.year));
        }
               
        public static DateTime IntToDateTime(this int p, Freq f)
        {
            if (f == Freq.day)
            {
                return new DateTime(1970, 1, 1).AddDays(p - 1);
            }
            else if (f == Freq.month)
            {
                return new DateTime(1970, 1, 1).AddMonths(p - 1).GetLastDayOfMonth(0);
            }
            else if (f == Freq.quarter)
            {
                return new DateTime(1970, 1, 1).AddMonths((p * 3) - 3).GetLastDayOfQuarter(0);
            }
            else if (f == Freq.year)
            {
                return new DateTime(1970, 1, 1).AddMonths((p * 12) - 12).GetLastDayOfYear(0);
            }

            return new DateTime(1970, 1, 1);
        }


        public static int DateTimeToInt(this DateTime p, Freq f)
        {
            if (f == Freq.day)
            {
                return p.DailyDifference(new DateTime(1970, 1, 1)) + 1;
            }
            else if (f == Freq.month)
            {
                return p.MonthlyDifference(new DateTime(1970, 1, 1)) + 1;
            }
            else if (f == Freq.quarter)
            {
                return p.QuarterlyDifference(new DateTime(1970, 1, 1)) + 1;
            }
            else if (f == Freq.year)
            {
                return p.YearlyDifference(new DateTime(1970, 1, 1)) + 1;
            }

            return 0;
        }

        public static bool IsFullPeriodDown(this DateTime p, Freq f)
        {
            if (f == Freq.day)
                return true;
            else if (f == Freq.month)
                return p.Equals(p.GetFirstDayOfMonth(0));
            else if (f == Freq.quarter)
                return p.Equals(p.GetFirstDayOfQuarter(0));
            else if (f == Freq.year)
                return p.Equals(p.GetFirstDayOfYear(0));

            return false;
        }

        public static bool IsFullPeriodUp(this DateTime p, Freq f)
        {
            if (f == Freq.day)
                return true;
            else if (f == Freq.month)
                return p.Equals(p.GetLastDayOfMonth(0));
            else if (f == Freq.quarter)
                return p.Equals(p.GetLastDayOfQuarter(0));
            else if (f == Freq.year)
                return p.Equals(p.GetLastDayOfYear(0));

            return false;
        }

        public static int DaysInQuarter(this DateTime d)
        {
            int diff = d.GetLastDayOfQuarter(0).DailyDifference(d.GetFirstDayOfQuarter(0)) + 1;

            return diff;
        }

        public static DateTime GetLastDayOfPeriod(this DateTime dateTime, Freq f, int h)
        {
            if (f == Freq.day)
            {
                return dateTime.AddDays(h);
            }
            else if (f == Freq.month)
            {
                return dateTime.GetLastDayOfMonth(h);
            }
            else if (f == Freq.quarter)
            {
                return dateTime.GetLastDayOfQuarter(h);
            }
            else if (f == Freq.year)
            {
                return dateTime.GetLastDayOfYear(h);
            }

            return new DateTime();
        }

        public static int NumberOfLowerPeriodsInHigher(this DateTime d, Freq l, Freq h)
        {
            int diff = 0;

            if (l == Freq.day)
            {
                if (h == Freq.month)
                {
                    diff = DateTime.DaysInMonth(d.Year, d.Month);
                }
                else if (h == Freq.quarter)
                {
                    diff = d.GetLastDayOfQuarter(0).DailyDifference(d.GetFirstDayOfQuarter(0)) + 1;
                }
                else if (h == Freq.year)
                {
                    diff = d.GetLastDayOfYear(0).DailyDifference(d.GetFirstDayOfYear(0)) + 1;
                }
            }
            else if (l == Freq.month)
            {
                if (h == Freq.quarter)
                {
                    diff = 3;
                }
                else if (h == Freq.year)
                {
                    diff = 12;
                }
            }
            else if (l == Freq.quarter)
            {
                if (h == Freq.year)
                {
                    diff = 4;
                }
            }

            return diff;
        }

        public static int DaysUntilNext29February(this DateTime d)
        {
            int year = d.Year;

            while (true)
            {
                if (DateTime.IsLeapYear(year))
                {
                    if (d <= new DateTime(year, 02, 29))
                    {
                        return new DateTime(year, 02, 29).DailyDifference(d);
                    }
                }

                year = year + 1;
            }            
        }

        public static int DaysSincePrevious29February(this DateTime d)
        {
            int year = d.Year;

            while (true)
            {
                if (DateTime.IsLeapYear(year))
                {
                    if (d >= new DateTime(year, 02, 29))
                    {
                        return d.DailyDifference(new DateTime(year, 02, 29));
                    }
                }

                year = year - 1;
            }
        }

        public static Tuple<bool, int?, int?> GetNoNaNRange(this Tseries a)
        {
            if (a == null || a.Obs.Length == 0)
            {
                return new Tuple<bool, int?, int?>(false, null, null);
            }

            int firstNonNaN = 0;
            int lastNonNaN = a.Obs.GetUpperBound(0);

            for (int i = 0; i <= a.Obs.GetUpperBound(0); i++)
            {
                if (!double.IsNaN(a.Obs[i]))
                {
                    firstNonNaN = i;
                    break;
                }
            }

            for (int i = a.Obs.GetUpperBound(0); i >= 0; i--)
            {
                if (!double.IsNaN(a.Obs[i]))
                {
                    lastNonNaN = i;
                    break;
                }
            }

            return new Tuple<bool, int?, int?>(true, firstNonNaN, lastNonNaN);
        }

        public static double[] GetObs(this Tseries a, DateTime beg, DateTime end)
        {
            int ibeg = beg.DateTimeToInt(a.Freq);
            int iend = end.DateTimeToInt(a.Freq);

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;

            if (a.IsConstant)
            {
                for (int i = 0; i < obs.Length; i++)
                {
                    obs[idx] = double.NaN;
                    idx++;
                }

                return obs;
            }

            idx = 0;

            for (int i = 0; i < obs.Length; i++)
            {
                obs[idx] = double.NaN;
                idx++;
            }

            if (ibeg > a.LastPeriod || iend < a.FirstPeriod)
            {
                return obs;
            }

            int copyStartIdx = 0;
            int pasteStartIdx = 0;

            if (ibeg > a.FirstPeriod)
            {
                copyStartIdx = ibeg - a.FirstPeriod;
            }
            else
            {
                pasteStartIdx = a.FirstPeriod - ibeg;
            }

            int copyLenght = 0;

            copyLenght = Math.Min(iend, a.LastPeriod) - Math.Max(a.FirstPeriod + copyStartIdx, ibeg) + 1;
            
            Array.Copy(a.Obs, copyStartIdx, obs, pasteStartIdx, copyLenght);

            return obs;
        }

        public static DateTime TryParseDate(this string s, bool firstPeriod)
        {
            //Match dates e.g. 31.01.1970 
            Regex regex1 = new Regex(@"^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)[1-2][0-9]{3}$", RegexOptions.IgnoreCase);

            Match match1 = regex1.Match(s);
            if (match1.Success)
            {
               return DateTime.ParseExact(match1.Value, "dd.MM.yyyy", new CultureInfo("de-DE"));
            }

            //Match months
            Regex regex2 = new Regex(@"(?<year>^[1-2][0-9]{3})m(?<month>([1-9]$|1[0-2]$|0[1-9]$))", RegexOptions.IgnoreCase);

            Match match2 = regex2.Match(s);
            if (match2.Success)
            {
                string year = match2.Groups["year"].Value;
                string month = match2.Groups["month"].Value;

                DateTime dt = new DateTime(int.Parse(year), int.Parse(month), 1);

                return firstPeriod ? dt : dt.GetLastDayOfMonth(0);
            }

            //Match quarters
            Regex regex3 = new Regex(@"(?<year>^[1-2][0-9]{3})q(?<quarter>([1-4]$|0[1-4]$)$)", RegexOptions.IgnoreCase);

            Match match3 = regex3.Match(s);
            if (match3.Success)
            {
                string year = match3.Groups["year"].Value;
                string quarter = match3.Groups["quarter"].Value;

                DateTime dt = new DateTime(int.Parse(year), int.Parse(quarter) * 3, 1);

                return firstPeriod ? dt.GetFirstDayOfQuarter(0) : dt.GetLastDayOfQuarter(0);
            }

            //Match years
            Regex regex4 = new Regex(@"(?<year>^[1-2][0-9]{3}$)", RegexOptions.IgnoreCase);

            Match match4 = regex4.Match(s);
            if (match4.Success)
            {
                string year = match4.Groups["year"].Value;

                DateTime dt = new DateTime(int.Parse(year), 1, 1);

                return firstPeriod ? dt : dt.GetLastDayOfYear(0);
            }

            throw new Exception("Couldn't recognise the date \"" + s + "\"");
        }
    }
}




//public static ValueTuple<int, int> NonNaNRange(this double[] a)
//{
//    if (a == null || a.Length == 0)
//    {
//        return new ValueTuple<int, int>(-1, -1);
//    }

//    int firstNonNaN = 0;
//    int lastNonNaN = a.GetUpperBound(0);

//    for (int i = 0; i <= a.GetUpperBound(0); i++)
//    {
//        if (!double.IsNaN(a[i]))
//        {
//            firstNonNaN = i;
//            break;
//        }
//    }

//    for (int i = a.GetUpperBound(0); i >= 0; i--)
//    {
//        if (!double.IsNaN(a[i]))
//        {
//            lastNonNaN = i;
//            break;
//        }
//    }

//    return new ValueTuple<int, int>(firstNonNaN, lastNonNaN);

//}