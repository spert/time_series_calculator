﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesModels
{
    public class Tseries
    {
        readonly int _id;
        readonly int _firstPeriod;
        readonly int _lastPeriod;
        readonly int _firstPeriodXNaN;
        readonly int _lastPeriodXNaN;
        readonly Freq _freq;
        readonly double[] _obs;
        readonly bool _isConstant;
 
        public Tseries(double obs)
        {
            _id = int.MinValue;
            _freq = Freq.constant;           
            _firstPeriod = int.MinValue;
            _lastPeriod = int.MaxValue;
            _isConstant = true;  
            _obs = new double[1] { obs };            
        }

        public Tseries(int id, int firstPeriod, Freq freq, double[] obs)
        {
            _id = id;
            _freq = freq;
            _obs = obs;
            _firstPeriod = firstPeriod;
            _lastPeriod = _firstPeriod + _obs.GetUpperBound(0);
            _isConstant = false;            
        }
        
        public bool IsConstant { get { return _isConstant; } }
        public int Id { get { return _id; } }
        public int FirstPeriod { get { return _firstPeriod; } }
        public int LastPeriod { get { return _lastPeriod; } }
        public DateTime FirstPeriodDate { get { return _firstPeriod.IntToDateTime(_freq); } }
        public DateTime LastPeriodDate { get { return _lastPeriod.IntToDateTime(_freq); } }
        public Freq Freq { get { return _freq; } }
        public double[] Obs { get { return _obs; } }

        public override string ToString()
        {
            string s = " =========== TIME SERIES =========== " + Environment.NewLine;
            s = s + "Series ID:  " + _id.ToString() + Environment.NewLine;
            s = s + "Frequency:  " + _freq.ToString() + Environment.NewLine;
            s = s + "First period number:  " + _firstPeriod.ToString() + Environment.NewLine;
            s = s + "First period date:  " + FirstPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "Last period date:  " + LastPeriodDate.ToString("dd.MM.yyyy") + Environment.NewLine;
            s = s + "# of observations:  " + _obs.Length.ToString() + Environment.NewLine;
            s = s + Environment.NewLine;
            s = s + "Time series values:" + Environment.NewLine;

            for (int i = 0; i < _obs.Length; i++)
            {
                if (i < 3 && _obs.Length >= 7)
                {
                    s = s + i.ToString() + " : " + (_firstPeriod + i).IntToDateTime(_freq).ToString("dd.MM.yyyy") + " : " + _obs[i] + Environment.NewLine;
                }

                if (i == 3 && _obs.Length >= 7)
                {
                    s = s + "..." + Environment.NewLine;
                }

                if (i > _obs.Length - 4 && _obs.Length >= 7)
                {
                    s = s + i.ToString() + " : " + (_firstPeriod + i).IntToDateTime(_freq).ToString("dd.MM.yyyy") + " : " + _obs[i] + Environment.NewLine;
                }

                if (_obs.Length < 7)
                {
                    s = s + i.ToString() + " : " + (_firstPeriod + i).IntToDateTime(_freq).ToString("dd.MM.yyyy") + " : " + _obs[i] + Environment.NewLine;
                }
            }

            return s;

        }

    }
}
