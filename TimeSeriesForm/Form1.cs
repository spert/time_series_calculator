﻿using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeSeriesCalculator;
using TimeSeriesDataAccess;
using TimeSeriesModels;

namespace TimeSeriesForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void BtnEvaluate_Click(object sender, EventArgs e)
        { 

            try
            {
                txtResult.Text = "";

                Ninject.IKernel kernal = new StandardKernel();
                kernal.Bind<IRepository<Tseries>>().To<SQLRepository>();
                kernal.Bind<IService>().To<CalculatorService>();
                var instance = kernal.Get<CalculatorService>();
                var result = await instance.EvaluateExpression(txtExpression.Text);

                txtResult.Text = result.ToString();

            }
            catch (Exception ex)
            {
                txtResult.Text = ex.Message.ToString();
            }
        }
    }
}
