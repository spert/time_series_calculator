﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeSeriesCalculator;
using TimeSeriesDataAccess;
using TimeSeriesModels;

namespace TimeSeriesForm
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository<TseriesOb>>().To<SQLRepository>();
            Bind<IService>().To<CalculatorService>();
        }
    }
}
