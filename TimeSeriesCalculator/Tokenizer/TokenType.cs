﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeSeriesCalculator
{
    public enum TokenType
    {
        Integer,
        FloatingPoint,
        FunctionName,
        FunctionArgument,
        Operation,
        LeftBracket,
        RightBracket,
        ArgumentSeparator
    }
}
