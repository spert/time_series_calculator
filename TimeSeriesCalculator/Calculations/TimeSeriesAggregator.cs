﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries FREQ(Tseries a, string freq, string transf)
        {
            string _freq = freq.Trim('"');
            string _transf = transf.Trim('"');

            //Match frequency 
            Freq h = Freq.undetermined;

            Regex regex1 = new Regex(@"^(?<freq>(day|month|quarter|year))$", RegexOptions.IgnoreCase);

            Match match1 = regex1.Match(_freq);
            if (match1.Success)
            {
                Enum.TryParse(match1.Groups["freq"].Value, out h);
            }
            else
            {
                throw new ParseException(string.Format("Invalid frequency \"{0}\" detected.", freq));
            }

            //Match transformation 
            Transf t = Transf.undetermined;

            Regex regex2 = new Regex(@"^(?<transf>(sum|average|max|min|first|last))$", RegexOptions.IgnoreCase);

            Match match2 = regex2.Match(_transf);
            if (match2.Success)
            {
                Enum.TryParse(match2.Groups["transf"].Value, out t);
            }
            else
            {
                throw new ParseException(string.Format("Invalid transformation \"{0}\" detected.", transf));
            }

            if (!a.IsConstant)
            {
                return AggregateToHigherFrequency(a, h, t);
            }

            return a;
        }


        private static Tseries AggregateToHigherFrequency(Tseries a, Freq h, Transf t)
        {
            // Frequency validation
            if (h < a.Freq)
            {
                throw new Exception("Aggregation to lower frequency is not avaliable");
            }
            else if (h == a.Freq)
            {
                return a;
            }
            
            DateTime dlowBeg = DateTime.MinValue;
            DateTime dLowEnd = DateTime.MaxValue;
            int highObsCount = 0;

            if (h == Freq.month)
            {
                Tuple<DateTime, DateTime, int, int>  diff = a.LastPeriodDate.MonthlyDifferenceFull(a.FirstPeriodDate);
                dlowBeg = diff.Item1.GetFirstDayOfMonth(0);
                dLowEnd = diff.Item2.GetLastDayOfMonth(0);
                highObsCount = diff.Item4 - diff.Item3 + 1;
            }
            else if (h == Freq.quarter)
            {
                Tuple<DateTime, DateTime, int, int>  diff = a.LastPeriodDate.QuarterlyDifferenceFull(a.FirstPeriodDate);
                dlowBeg = diff.Item1.GetFirstDayOfQuarter(0);
                dLowEnd = diff.Item2.GetLastDayOfQuarter(0);
                highObsCount = diff.Item4 - diff.Item3 + 1;
            }
            else if (h == Freq.year)
            {
                Tuple<DateTime, DateTime, int, int> diff = a.LastPeriodDate.YearlyDifferenceFull(a.FirstPeriodDate);
                dlowBeg = diff.Item1.GetFirstDayOfYear(0);
                dLowEnd = diff.Item2.GetLastDayOfYear(0);
                highObsCount = diff.Item4 - diff.Item3 + 1;
            }

            // Transformation validation

            if (t == Transf.average)
            {
                return AggregateByAverage(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else if (t == Transf.sum)
            {
                return AggregateBySum(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else if (t == Transf.max)
            {
                return AggregateByMax(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else if (t == Transf.min)
            {
                return AggregateByMin(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else if (t == Transf.first)
            {
                return AggregateByFirst(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else if (t == Transf.last)
            {
                return AggregateByLast(a, h, dlowBeg, dLowEnd, highObsCount);
            }
            else
                throw new Exception("Unsuitable frequency for aggregation");
        }

        private static Tseries AggregateByAverage(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int lowerCounter = 0;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = lowerObs + d;
                    lowerCounter++;
                }
                else if (double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = d;
                    lowerCounter++;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    if (!double.IsNaN(lowerObs))
                    {
                        higherObs[higherCounter] = lowerObs / lowerCounter;
                    }
                    else
                    {
                        higherObs[higherCounter] = lowerObs;
                    }                    

                    higherCounter++;
                    lowerCounter = 0;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }

        private static Tseries AggregateBySum(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = lowerObs + d;
                 }
                else if (double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = d;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    higherObs[higherCounter] = lowerObs;

                    higherCounter++;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }

        private static Tseries AggregateByMax(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = Math.Max(lowerObs, d);
                }
                else if (double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = d;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    higherObs[higherCounter] = lowerObs;

                    higherCounter++;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }

        private static Tseries AggregateByMin(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = Math.Min(lowerObs, d);
                }
                else if (double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = d;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    higherObs[higherCounter] = lowerObs;

                    higherCounter++;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }
        private static Tseries AggregateByFirst(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (double.IsNaN(lowerObs) && !double.IsNaN(d))
                {
                    lowerObs = d;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    higherObs[higherCounter] = lowerObs;

                    higherCounter++;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }

        private static Tseries AggregateByLast(Tseries a, Freq h, DateTime dLowBeg, DateTime dLowEnd, int highObsCount)
        {
            double lowerObs = double.NaN;
            int higherCounter = 0;

            int intLowBeg = dLowBeg.DateTimeToInt(a.Freq) - a.FirstPeriod;
            int intLowEnd = dLowEnd.DateTimeToInt(a.Freq) - a.FirstPeriod;

            int lowerPeriodsInHigher = dLowBeg.NumberOfLowerPeriodsInHigher(a.Freq, h);
            double[] higherObs = new double[highObsCount];

            for (int i = intLowBeg; i <= intLowEnd; i++)
            {
                double d = a.Obs[i];

                if (!double.IsNaN(d))
                {
                    lowerObs = d;
                }

                lowerPeriodsInHigher--;

                if (lowerPeriodsInHigher == 0)
                {
                    higherObs[higherCounter] = lowerObs;

                    higherCounter++;
                    lowerObs = double.NaN;

                    lowerPeriodsInHigher = dLowBeg.GetLastDayOfPeriod(h, higherCounter).NumberOfLowerPeriodsInHigher(a.Freq, h);
                }
            }

            return new Tseries(a.Id, dLowBeg.DateTimeToInt(h), h, higherObs);
        }
    }
}
