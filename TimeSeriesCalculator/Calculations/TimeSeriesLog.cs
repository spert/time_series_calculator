﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries Log(Tseries a)
        {
            if (a.IsConstant)
            {
                return LogConstant(a);
            }
            else if (!a.IsConstant)
            {
                return LogObject(a);
            }
            else
            {
                throw new Exception("Unexpected type" + a.GetType().ToString());
            }
        }

        public static Tseries LogObject(Tseries a)
        {
            double[] obs = new double[a.LastPeriod - a.FirstPeriod + 1];

            int idx = 0;

            for (int i = 0; i < a.Obs.Length; i++)
            {
                double d = a.Obs[idx];

                if (double.IsNaN(d) || d <= 0)
                {
                    obs[idx] = double.NaN;
                }
                else
                {
                    obs[idx] = Math.Log(d);
                }                

                idx++;
            }

            return new Tseries(a.Id, a.FirstPeriod, a.Freq, obs);

        }

        public static Tseries LogConstant(Tseries a)
        {
            if (double.IsNaN(a.Obs[0]) || a.Obs[0] <= 0)
            {
                return new Tseries(double.NaN);
            }
            else
            {
                return new Tseries(Math.Log(a.Obs[0]));
            }            
        }
    }
}
