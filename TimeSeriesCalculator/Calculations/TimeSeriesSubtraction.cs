﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries Subtraction(Tseries a, Tseries b)
        {
            if (!a.IsConstant && !b.IsConstant)
            {
                return SubtractionObOb(a, b);
            }
            else if (a.IsConstant && !b.IsConstant)
            {
                return SubtractionObConst(a, b);
            }
            else if (!a.IsConstant && b.IsConstant)
            {
                return SubtractionObConst(a, b);
            }
            else if (a.IsConstant && b.IsConstant)
            {
                return SubtractionConstConst(a, b);
            }
            else
            {
                throw new Exception("Unexpected type" + a.GetType().ToString());
            } 
        }

        public static Tseries SubtractionObOb(Tseries a, Tseries b)
        {
            int ibeg = Math.Max(a.FirstPeriod, b.FirstPeriod);
            int iend = Math.Min(a.LastPeriod, b.LastPeriod);

            if (ibeg > iend)
            {
                return new Tseries(new int(), ibeg, a.Freq, new double[0]);
            }

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;
            int idxa = ibeg - a.FirstPeriod;
            int idxb = ibeg - b.FirstPeriod;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                obs[idx] = a.Obs[idxa] - b.Obs[idxb];

                idx++;
                idxa++;
                idxb++;
            }

            return new Tseries(new int(), ibeg, a.Freq, obs);

        }

        public static Tseries SubtractionObConst(Tseries a, Tseries b)
        {
            int ibeg = a.FirstPeriod;
            int iend = a.LastPeriod;

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                obs[idx] = a.Obs[idx] - b.Obs[0];

                idx++;
            }

            return new Tseries(new int(), ibeg, a.Freq, obs);
        }

        public static Tseries SubtractionConstOb(Tseries a, Tseries b)
        {
            int ibeg = b.FirstPeriod;
            int iend = b.LastPeriod;

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                obs[idx] =  a.Obs[0] - b.Obs[idx];

                idx++;
            }

            return new Tseries(new int(), ibeg, b.Freq, obs);
        }

        public static Tseries SubtractionConstConst(Tseries a, Tseries b)
        {
            return new Tseries(a.Obs[0] - b.Obs[0]);
        }

    }
}
