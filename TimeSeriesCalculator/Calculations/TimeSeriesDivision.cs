﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries Division(Tseries a, Tseries b)
        {
            if (!a.IsConstant && !b.IsConstant)
            {
                return DivisionObOb(a, b);
            }
            else if (a.IsConstant && !b.IsConstant)
            {
                return DivisionConstOb(a,b);
            }
            else if (!a.IsConstant && b.IsConstant)
            {
                return DivisionObConst(a, b);
            }
            else if(a.IsConstant && b.IsConstant)
            { 
                return DivisionConstConst(a, b);
            }
            else
            {
                throw new Exception("Unexpected type" + a.GetType().ToString());
            }
        }

        public static Tseries DivisionObOb(Tseries a, Tseries b)
        {

            int ibeg = Math.Max(a.FirstPeriod, b.FirstPeriod);
            int iend = Math.Min(a.LastPeriod, b.LastPeriod);

            if (ibeg > iend)
            {
                return new Tseries(new int(), ibeg, a.Freq, new double[0]);
            }

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;
            int idxa = ibeg - a.FirstPeriod;
            int idxb = ibeg - b.FirstPeriod;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                if (double.IsNaN(b.Obs[idxb]) || b.Obs[idxb] == 0 )
                {
                    obs[idx] = double.NaN;
                }
                else
                {
                    obs[idx] = a.Obs[idxa] / b.Obs[idxb];
                }                    

                idx++;
                idxa++;
                idxb++;
            }

            return new Tseries(new int(), ibeg, a.Freq, obs);

        }

        public static Tseries DivisionObConst(Tseries a, Tseries b)
        {
            int ibeg = a.FirstPeriod;
            int iend = a.LastPeriod;

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                if (double.IsNaN(b.Obs[0]) || b.Obs[0] == 0)
                {
                    obs[idx] = double.NaN;
                }
                else
                {
                    obs[idx] = a.Obs[idx] / b.Obs[0];
                }

                idx++;
            }

            return new Tseries(new int(), ibeg, a.Freq, obs);
        }

        public static Tseries DivisionConstOb(Tseries a, Tseries b)
        {
            int ibeg = b.FirstPeriod;
            int iend = b.LastPeriod;

            double[] obs = new double[iend - ibeg + 1];

            int idx = 0;

            for (int i = 0; i < iend - ibeg + 1; i++)
            {
                if (double.IsNaN(b.Obs[idx]) || b.Obs[idx] == 0)
                {
                    obs[idx] = double.NaN;
                }
                else
                {
                    obs[idx] = a.Obs[0] / b.Obs[idx];
                }

                idx++;
            }

            return new Tseries(new int(), ibeg, b.Freq, obs);
        }

        public static Tseries DivisionConstConst(Tseries a, Tseries b)
        {
            if (double.IsNaN(b.Obs[0]) || b.Obs[0] == 0)
            {
                return new Tseries(double.NaN);  
            }
            else
            {
                return new Tseries(a.Obs[0] / b.Obs[0]);
            }           
        }
    }
}
