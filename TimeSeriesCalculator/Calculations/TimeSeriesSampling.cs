﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries SampleExclNaN(Tseries a, string beg, string end)
        {
            DateTime dbeg = beg.Trim('"').TryParseDate(true);
            DateTime dend = end.Trim('"').TryParseDate(false);

            int ibeg = dbeg.GetLastDayOfPeriod(a.Freq, 0).DateTimeToInt(a.Freq);
            int iend = dend.GetLastDayOfPeriod(a.Freq, 0).DateTimeToInt(a.Freq);

            if (ibeg > iend)
            {
                throw new Exception("Beginning of the sample can't be a later date than the end date of the sample");
            }

            if (dbeg < new DateTime(1970, 1, 1))
            {
                throw new Exception("Sample beginning is out of range 1970-2050: " + "\"" + beg + "\"");
            }

            if (dend > new DateTime(2050, 12, 31))
            {
                throw new Exception("Sample end is out of range 1970-2050: " + "\"" + end + "\"");
            }

            if (ibeg + (iend - ibeg) < a.FirstPeriod || ibeg > a.FirstPeriod + a.Obs.Length - 1)
            {
                return new Tseries(a.Id, ibeg, a.Freq, new double[0]);
            }

            int ibegmin = Math.Max(ibeg, a.FirstPeriod);
            int iendmin = Math.Min(iend, a.FirstPeriod + a.Obs.Length - 1);

            double[] obs = new double[iendmin - ibegmin + 1];
            Array.Copy(a.Obs, ibegmin - a.FirstPeriod, obs, 0, iendmin - ibegmin + 1);

            return new Tseries(a.Id, ibegmin, a.Freq, obs);
        }

        public static Tseries SampleInclNaN(Tseries a, string beg, string end)
        {
            DateTime dbeg = beg.Trim('"',' ').TryParseDate(true);
            DateTime dend = end.Trim('"',' ').TryParseDate(false);

            if (dbeg > dend || dbeg < new DateTime(1970,1,1) || dend > new DateTime(2050, 12, 31)) 
            {
                throw new Exception("Beginning of the sample can't be a later date than the end date of the sample");
            }

            if (dbeg < new DateTime(1970, 1, 1) )
            {
                throw new Exception("Sample beginning is out of range 1970-2050: " + "\"" + beg + "\"");
            }

            if (dend > new DateTime(2050, 12, 31))
            {
                throw new Exception("Sample end is out of range 1970-2050: " + "\"" + end + "\"");
            }

            return new Tseries(a.Id, dbeg.DateTimeToInt(a.Freq), a.Freq, a.GetObs(dbeg, dend)); 
            
        }
    }
}
