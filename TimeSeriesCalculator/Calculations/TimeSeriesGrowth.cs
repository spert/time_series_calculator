﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;


namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries PCY(Tseries a)
        {
            if (a.IsConstant)
            {
                return new Tseries(0);
            }
            else if (!a.IsConstant)
            {
                return YoYGrowth(a, true);                
            }
            else
            {
                throw new Exception("Unexpected type" + a.GetType().ToString());
            }

        }

        public static Tseries PCHY(Tseries a)
        {
            if (a.IsConstant)
            {
                return new Tseries(0);                
            }
            else
            {
                return YoYGrowth(a, false);
            }
        }

        public static Tseries PC(Tseries a)
        {
            if (a.IsConstant)
            {
                return new Tseries(0);
            }
            else
            {
                if (a.Freq == Freq.day)
                {
                    return D_GrowthCalculation(a, 1, 100);
                }
                else
                {
                    return M_Q_Y_GrowthCalculation(a, 1, 100);
                }
            }
        }

        public static Tseries PCH(Tseries a)
        {
            if (a.IsConstant)
            {
                return new Tseries(0);
            }
            else
            {
                if (a.Freq == Freq.day)
                {
                    return D_GrowthCalculation(a, 1, 1);
                }
                else
                {
                    return M_Q_Y_GrowthCalculation(a, 1, 1);
                }                
            }
        }

        private static Tseries YoYGrowth(Tseries a, bool perc)
        {
            int nobs = 0;
            int h = 0;
            double m = 1;

            if (perc)
            {
                m = 100;
            }

            if (a.Freq == Freq.day)
            {
                if (a.FirstPeriodDate.Month <= 2 && DateTime.IsLeapYear(a.FirstPeriodDate.Year))
                {
                    nobs = a.Obs.Length - 366;
                }
                else
                {
                    nobs = a.Obs.Length - 365;
                }

                return D_YOYCalculation(a, nobs, m);
            }

            if (a.Freq == Freq.month)
            {
                h = 12;
            }
            else if (a.Freq == Freq.quarter)
            {
                h = 4;
            }
            else if (a.Freq == Freq.year)
            {
                h = 1;
            }

            return M_Q_Y_GrowthCalculation(a, h, m);
        }

        private static Tseries M_Q_Y_GrowthCalculation(Tseries a, int h, double m)
        {

            double[] growthObs = new double[a.Obs.Length - h];

            for (int i = h; i < a.Obs.Length; i++)
            {
                double d = a.Obs[i];
                double dp = a.Obs[i - h];

                if (!double.IsNaN(d) && !double.IsNaN(dp) && dp != 0)
                {
                    growthObs[i - h] = (d - dp) / dp * m;
                }
                else
                {
                    growthObs[i - h] = double.NaN;
                }
            }

            return new Tseries(new int(), a.FirstPeriod + h, a.Freq, growthObs);
        }

        private static Tseries D_YOYCalculation(Tseries a, int nobs, double m)
        {
            double[] growthObs = new double[nobs];

            int lag = 365;
            int dayOfNext29Feb = a.FirstPeriod + 365 + (a.FirstPeriod + 365).IntToDateTime(a.Freq).DaysUntilNext29February();
            int indexOfNext29Feb = dayOfNext29Feb - a.FirstPeriod - 365;
            int dayIndexToReduceLag = 0;

            for (int i = 365; i < a.Obs.Length; i++)
            {

                double d = a.Obs[i];
                double dp = a.Obs[i - lag];

                if (!double.IsNaN(d) && !double.IsNaN(dp) && dp != 0)
                {
                    growthObs[i - 365] = (d - dp) / dp * m;
                }
                else if (double.IsNaN(d) || dp == 0)
                {
                    growthObs[i - 365] = double.NaN;
                }
                else if (double.IsNaN(dp))
                {
                    growthObs[i - 365] = double.NaN;

                    for (int j = i - lag + 1; j < i - lag + 4; j++)
                    {
                        if (double.IsNaN(a.Obs[j]) && a.Obs[j] != 0)
                        {
                            growthObs[i - 365] = (d - a.Obs[j]) / a.Obs[j] * m;
                        }
                    }
                }

                if (indexOfNext29Feb == i - 365)
                {
                    lag = 366;

                    dayIndexToReduceLag = i + 365;
                    dayOfNext29Feb = a.FirstPeriod + i + (a.FirstPeriod + i + 1).IntToDateTime(a.Freq).DaysUntilNext29February() + 1;
                    indexOfNext29Feb = dayOfNext29Feb - a.FirstPeriod - 365;

                }

                if (dayIndexToReduceLag == i)
                {
                    lag = 365;
                }
            }

            return new Tseries(new int(), a.FirstPeriod + 365, a.Freq, growthObs);

        }

        private static Tseries D_GrowthCalculation(Tseries a, int h, double m)
        {
            double[] growthObs = new double[a.Obs.Length - h];

            for (int i = h; i < a.Obs.Length; i++)
            {
                double d = a.Obs[i];
                double dp = a.Obs[i - h];

                if (!double.IsNaN(d) && !double.IsNaN(dp) && dp != 0)
                {
                    growthObs[i - h] = (d - dp) / dp * m;
                }
                else if (double.IsNaN(d) || dp == 0)
                {
                    growthObs[i - h] = double.NaN;
                }
                else if (double.IsNaN(dp))
                {
                    growthObs[i - h] = double.NaN;

                    for (int j = i - h + 1; j < i - h + 4; j++)
                    {
                        if (double.IsNaN(a.Obs[j]) && a.Obs[j] != 0)
                        {
                            growthObs[i - h] = (d - a.Obs[j]) / a.Obs[j] * m;
                        }
                    }
                }
            }

            return new Tseries(new int(), a.FirstPeriod + h, a.Freq, growthObs);
        }
    }

}
