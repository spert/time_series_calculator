﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public partial class Calculator
    {
        public static Tseries UnaryMinus(Tseries a)
        {
            if (a.IsConstant)
            {
                return UnaryMinusConst(a);
            }
            else if (a is Tseries)
            { 
                return UnaryMinus(a);
            }
            else
            {
                throw new Exception("Unexpected type" + a.GetType().ToString());
            }
        }

        public static Tseries UnaryMinusOb(Tseries a)
        {
            double[] obs = new double[a.LastPeriod - a.FirstPeriod + 1];

            int idx = 0;

            for (int i = 0; i < a.Obs.Length; i++)
            { 
                obs[idx] = -1 * a.Obs[idx];              

                idx++;
            }

            return new Tseries(new int(), a.FirstPeriod, a.Freq, obs);

        }

        public static Tseries UnaryMinusConst(Tseries a)
        {
            return new Tseries(-a.Obs[0]);
        }
    }
}
