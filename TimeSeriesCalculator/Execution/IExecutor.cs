﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    public interface IExecutor
    {
        Tseries Execute(Operation operation, IFunctionRegistry functionRegistry, IConstantRegistry constantRegistry);
        Tseries Execute(Operation operation, IFunctionRegistry functionRegistry, IConstantRegistry constantRegistry, IDictionary<string, Tseries> variables);

        Func<IDictionary<string, Tseries>, Tseries> BuildFormula(Operation operation, IFunctionRegistry functionRegistry, IConstantRegistry constantRegistry);
    }
}
