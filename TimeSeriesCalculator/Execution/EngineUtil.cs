﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeSeriesModels;

namespace TimeSeriesCalculator
{
    /// <summary>
    /// Utility methods of Jace.NET that can be used throughout the engine.
    /// </summary>
    internal  static class EngineUtil
    {
        static internal IDictionary<string, Tseries> ConvertVariableNamesToLowerCase(IDictionary<string, Tseries> variables)
        {
            Dictionary<string, Tseries> temp = new Dictionary<string, Tseries>();
            foreach (KeyValuePair<string, Tseries> keyValuePair in variables)
            {
                temp.Add(keyValuePair.Key.ToLowerInvariant(), keyValuePair.Value);
            }

            return temp;
        }
    }
}
