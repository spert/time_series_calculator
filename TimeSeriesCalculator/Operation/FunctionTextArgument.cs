﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesCalculator
{
    public class FunctionTextArgument : Operation
    {
        public FunctionTextArgument(string name)
            : base(DataType.FuncParameter, true)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public override bool Equals(object obj)
        {
            FunctionTextArgument other = obj as FunctionTextArgument;
            if (other != null)
            {
                return this.Name.Equals(other.Name);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
