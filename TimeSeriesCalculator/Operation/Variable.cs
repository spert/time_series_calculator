﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeSeriesCalculator
{
    /// <summary>
    /// Represents a variable in a mathematical formula.
    /// </summary>
    public class FunctionTimeSeriesArgument : Operation
    {
        public FunctionTimeSeriesArgument(string name)
            : base(DataType.TimeSeries, true)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public override bool Equals(object obj)
        {
            FunctionTimeSeriesArgument other = obj as FunctionTimeSeriesArgument;
            if (other != null)
            {
                return this.Name.Equals(other.Name);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
