﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeSeriesCalculator
{
    public enum DataType
    {
        Integer,
        FloatingPoint,
        TimeSeries,
        FuncParameter
    }
}
