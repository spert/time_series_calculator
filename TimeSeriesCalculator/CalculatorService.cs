﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using TimeSeriesDataAccess;
using TimeSeriesModels;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace TimeSeriesCalculator
{
    public class CalculatorService : IService
    {
        IRepository<Tseries> _repo;

        public CalculatorService(IRepository<Tseries> repo)
        {
            _repo = repo;
        }

        public async Task<Tseries> EvaluateExpression(string expression)
        { 

            expression = expression.Replace(" ", "").Trim('=');                       

            string pattern = @"ts:[0-9]{7}";
   
            MatchCollection matchList = Regex.Matches(expression, pattern, RegexOptions.IgnoreCase);
            var lst = matchList.Cast<Match>().Select(match => match.Value).Distinct().ToList();

            Dictionary<string, Tseries> dictTso = new Dictionary<string, Tseries>();

            if (lst.Count > 0)
            {
                List<Tseries> lstTso = await _repo.All(lst).ConfigureAwait(false);                
                dictTso = lstTso.ToDictionary(x => "ts:" + x.Id.ToString(), x => (Tseries)x); 
            }                                                                  
            
            CalculationEngine engine = new CalculationEngine(CultureInfo.InvariantCulture, ExecutionMode.Interpreted, true, false, true);

            Tseries result = engine.Calculate(expression, dictTso);

            return result;

        }

    }
}
