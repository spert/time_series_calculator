using TimeSeriesCalculator;
using NUnit.Framework;
using System;
using System.Diagnostics;
using TimeSeriesModels;


namespace CalculatorTests
{
    public class SamplingTests
    {
        TseriesDict DictDaily;
        TseriesDict DictMonthly;
        TseriesDict DictQuarterly;
        TseriesDict DictYearly;

        Tseries Daily;
        Tseries Monthly;
        Tseries Quarterly;
        Tseries Yearly;


        [SetUp]
        public void Setup()
        {
            DictDaily = DummySeries.GenerateDailyDummySeries(0);
            DictMonthly = DummySeries.GenerateMonthlyDummySeries(0);
            DictQuarterly = DummySeries.GenerateQuarterlyDummySeries(0);
            DictYearly = DummySeries.GenerateYearlyDummySeries(0);

            Daily = HelperMethods.ConvertDictSeriesToObjectSeries(DictDaily);
            Monthly = HelperMethods.ConvertDictSeriesToObjectSeries(DictMonthly);
            Quarterly = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterly);
            Yearly = HelperMethods.ConvertDictSeriesToObjectSeries(DictYearly);
        }

        [Test]
        public void TimeSeriesSampleExclNaNTest()
        {
            Tseries y1 = Calculator.SampleExclNaN(Yearly, "1972", "1972");
            Assert.AreEqual(y1.Obs[0], 550.094450819672,"1");

            Tseries m1 = Calculator.SampleExclNaN(Monthly, "01.01.1970", "04.05.1978");
            Assert.AreEqual(m1.Obs[0], 676.465428571429, "2");
            Assert.AreEqual(m1.Obs[51], 329.4205, "3");

            Tseries m2 = Calculator.SampleExclNaN(Monthly, "01.01.1975", "04.05.1978");
            Assert.AreEqual(m1.Obs[0], 676.465428571429, "4"); 
        }

        [Test]
        public void TimeSeriesSampleInclNaNTest()
        {
            Tseries m1 = Calculator.SampleInclNaN(Monthly, "01.01.1970", "01.03.1970");
            Assert.AreEqual(m1.Obs[0], double.NaN, "1");
            Assert.AreEqual(m1.Obs[2], 568.313516129032, "2");

            Tseries m2 = Calculator.SampleInclNaN(Monthly, "1970M1", "1970M3");
            Assert.AreEqual(m2.Obs[0], double.NaN, "3");
            Assert.AreEqual(m2.Obs[2], 568.313516129032, "4");

            Tseries q1 = Calculator.SampleInclNaN(Quarterly, "1973q1", "1975q01");
            Assert.AreEqual(q1.Obs[0], 548.107622222222, "5");
            Assert.AreEqual(q1.Obs[5], 599.553029411765, "6");

            Tseries y1 = Calculator.SampleInclNaN(Yearly, "1974", "1976");
            Assert.AreEqual(y1.Obs[0], 533.787088709677, "7");

        }             

        [Test]
        public void Tseries_FirstPeriod_LastPeriod_Tests()
        {
            Assert.AreEqual(Daily.FirstPeriod, new DateTime(1970, 2, 15).DateTimeToInt(Freq.day), "1");
            Assert.AreEqual(Daily.LastPeriod, new DateTime(1974, 5, 4).DateTimeToInt(Freq.day), "2");
            Assert.AreEqual(Monthly.FirstPeriod, new DateTime(1970, 2, 28).DateTimeToInt(Freq.month), "3");
            Assert.AreEqual(Monthly.LastPeriod, new DateTime(1974, 5, 31).DateTimeToInt(Freq.month), "4");

            Assert.AreEqual(Daily.FirstPeriodDate, new DateTime(1970, 2, 15), "5");
            Assert.AreEqual(Daily.LastPeriodDate, new DateTime(1974, 5, 4), "6");
            Assert.AreEqual(Monthly.FirstPeriodDate, new DateTime(1970, 2, 28), "7");
            Assert.AreEqual(Monthly.LastPeriodDate, new DateTime(1974, 5, 31), "8");
        }

        [Test]
        public void Tseries_GetObs_Tests()
        {
            double[] d1 = Daily.GetObs(new DateTime(1970, 2, 23), new DateTime(1970, 3, 9));
            Assert.AreEqual(d1[0], 681.625,"1");
            Assert.AreEqual(d1[14], 321.185,"2");

            double[] d2 = Daily.GetObs(new DateTime(1970, 2, 10), new DateTime(1970, 3, 9));
            Assert.AreEqual(d2[0], double.NaN,"3");
            Assert.AreEqual(d2[27], 321.185, "4");

            double[] d3 = Daily.GetObs(new DateTime(1970, 2, 10), new DateTime(1974, 5, 6));
            Assert.AreEqual(d3[0], double.NaN, "5");
            Assert.AreEqual(d3[1544], 417.381, "6");

            double[] d4 = Daily.GetObs(new DateTime(1974, 4, 24), new DateTime(1974, 4, 24));
            Assert.AreEqual(d4[0], 274.987, "7");

            double[] m1 = Monthly.GetObs(new DateTime(1970, 12, 31), new DateTime(1973, 3, 31));
            Assert.AreEqual(m1[0], 566.267161290323, "7");
            Assert.AreEqual(m1[27], 534.262258064516, "8");

            double[] m2 = Monthly.GetObs(new DateTime(1970, 1, 1), new DateTime(1970, 4, 30));
            Assert.AreEqual(m2[0], double.NaN, "9");
            Assert.AreEqual(m2[3], 510.459566666667, "10");

            double[] m3 = Monthly.GetObs(new DateTime(1970, 1, 1), new DateTime(1974, 6, 30));
            Assert.AreEqual(m3[0], double.NaN, "11");
            Assert.AreEqual(m3[53], double.NaN, "12");

        }
    }
}