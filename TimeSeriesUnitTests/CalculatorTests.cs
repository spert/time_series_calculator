﻿using Ninject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using TimeSeriesCalculator;
using TimeSeriesDataAccess;
using TimeSeriesModels;

namespace TimeSeriesUnitTests
{

    public class TseriesTests
    {
        TseriesDict DictDaily;
        TseriesDict DictMonthly;
        TseriesDict DictQuarterly;
        TseriesDict DictYearly;

        Tseries Daily;
        Tseries Monthly;
        Tseries Quarterly;
        Tseries Yearly;

        CalculatorService serv;

        [SetUp]
        public void Setup()
        {
            DictDaily = DummySeries.GenerateDailyDummySeries(0);
            DictMonthly = DummySeries.GenerateMonthlyDummySeries(1);
            DictQuarterly = DummySeries.GenerateQuarterlyDummySeries(2);
            DictYearly = DummySeries.GenerateYearlyDummySeries(3);

            Daily = HelperMethods.ConvertDictSeriesToObjectSeries(DictDaily);
            Monthly = HelperMethods.ConvertDictSeriesToObjectSeries(DictMonthly);
            Quarterly = HelperMethods.ConvertDictSeriesToObjectSeries(DictQuarterly);
            Yearly = HelperMethods.ConvertDictSeriesToObjectSeries(DictYearly);

            Ninject.IKernel kernal = new StandardKernel();
            kernal.Bind<IRepository<Tseries>>().To<SQLRepository>();
            kernal.Bind<IService>().To<CalculatorService>();
            serv = kernal.Get<CalculatorService>();
        }

        [Test]
        public void FrequencyConversionTest()
        {
            Tseries d1 = Calculator.FREQ(Daily, "month", "average");
            Assert.AreEqual(d1.Obs[0], 568.31351612903234,"1");
            Assert.AreEqual(d1.Obs[49], 635.5707, "2");

            Tseries d2 = Calculator.FREQ(Daily, "month", "sum");
            Assert.AreEqual(d2.Obs[0], 17617.719, "3");
            Assert.AreEqual(d2.Obs[49], 19067.121, "4");

            Tseries d3 = Calculator.SampleInclNaN(Daily, "1974M4", "1974M6");
            Tseries d4 = Calculator.FREQ(d3, "month", "average");
            //This test did not evaluate correctly
        }

        [Test]
        public void TimeSeriesMultiplicationTest()
        {
            Tseries m1 = Calculator.Multiplication(Monthly, Monthly);
            Assert.AreEqual(m1.Obs[0], 457605.47605232714, "1");
        }

        [Test]
        public void ConstantMultiplicationTest()
        {
            Tseries a = new Tseries(5);
            Tseries b = new Tseries(3);
            Tseries c = Calculator.Multiplication(a, b);
            Assert.AreEqual(c.Obs[0], 15, "1");
        }

        [Test]
        public async Task ExpressionEvaluationTest()
        {
            var s = await serv.EvaluateExpression("=log(\"ts:1234567\")");
            Assert.AreEqual(s.Obs[0], 6.5514188491696039, "1");
        }
    }
}