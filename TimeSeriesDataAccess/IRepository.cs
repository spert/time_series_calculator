﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSeriesModels;

namespace TimeSeriesDataAccess
{
    public interface IRepository<T> where T : class
    {
        Task<List<Tseries>> All(List<string> lst);
        Task<T> FindById(string Id);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);

    }

    
}
