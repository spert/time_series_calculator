﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSeriesModels;

namespace TimeSeriesDataAccess
{
    public class SQLRepository : IRepository<Tseries>
    {
        public SQLRepository()
        {

        }
        public async Task<List<Tseries>> All(List<string> lst)
        {
            List<Tseries> result = new List<Tseries>();

            foreach (var s in lst)
            {
                int id = int.Parse(s.Substring(3)); 

                Tseries ob = HelperMethods.ConvertDictSeriesToObjectSeries(DummySeries.GenerateDailyDummySeries(id));
                result.Add(ob);
            }

            await Task.Delay(1000);

            return result;
        }

        public Task<Tseries> FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public void Add(Tseries entity)
        {
            throw new NotImplementedException();
        }        

        public void Delete(Tseries entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Tseries entity)
        {
            throw new NotImplementedException();
        }      
    }
}
